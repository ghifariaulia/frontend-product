import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { Container } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CommentIcon from '@material-ui/icons/Comment';
import Carousel from 'react-bootstrap/Carousel';
import image1 from './assets/images/image1.jpg';
import image2 from './assets/images/image2.jpg';
import image3 from './assets/images/image3.jpg';
import coffee from './assets/images/coffee.jpg';

const useStyles = makeStyles((theme) => ({
  appBar: {
    backgroundColor: '#fff',
  },
  hero: {
    backgroundImage: `linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url('https://images.unsplash.com/photo-1558981852-426c6c22a060?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80')`,
    height: '500px',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    position: 'relative',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    color: '#fff',
    fontSize: '4rem',
    [theme.breakpoints.down('sm')]: {
      height: 300,
      fontSize: '3em',
    },
  },
  blogContainer: {
    paddingTop: theme.spacing(3),
  },
  blogTitle: {
    fontWeight: 800,
    paddingBottom: theme.spacing(3),
  },
  card: {
    maxWidth: '100%',
  },
  media: {
    height: 240,
  },
  cardActions: {
    display: 'flex',
    margin: '0 10px',
    justifyContent: 'space-between',
  },
  author: {
    display: 'flex',
  },
  carrouselSize: {
    width: '100%',
    height: '100%',
  },
}));

function App() {
  const classes = useStyles();
  return (
    <div className="App">
      <Carousel fade className={classes.carrouselSize}>
        <Carousel.Item className={classes.carrouselSize}>
          <img className="d-block w-100" src={image1} alt="First slide" />
          <Carousel.Caption>
            <h3>Drinks Couple</h3>
            <p>Taste this fresh drinks and get your free mini snack for your couple! </p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item className={classes.carrouselSize}>
          <img className="d-block w-100" src={image2} alt="Second slide" />

          <Carousel.Caption>
            <h3>Lychee Oolong Tea</h3>
            <p>Taste this Lychee Oolong Tea for your health and freshness!</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item className={classes.carrouselSize}>
          <img className="d-block w-100" src={image3} alt="Third slide" />

          <Carousel.Caption>
            <h3>Chocolate Lava</h3>
            <p>Taste the sweetness of this Chocolate Lava with your friend!</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
      <Container maxWidth="lg" className={classes.blogContainer}>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6} md={4}>
            <Card className={classes.card}>
              <CardActionArea>
                <CardMedia className={classes.media} image={coffee} title="Coffee" />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    MyCoffee
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    Promo minggu ini! MyCoffee diskon 50%!
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions className={classes.cardActions}>
                <Box className={classes.author}>
                  <Box ml={2}>
                    <Typography variant="subtitle2" component="p">
                      Johanes Simarmata
                    </Typography>
                    <Typography variant="subtitle2" color="textSecondary" component="p">
                      1 May 2021
                    </Typography>
                  </Box>
                </Box>
                <Box>
                  <CommentIcon></CommentIcon>
                </Box>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={12} sm={6} md={4}>
            <Card className={classes.card}>
              <CardActionArea>
                <CardMedia className={classes.media} image={coffee} title="Coffee" />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    MyCoffee2
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    Promo minggu depan! MyCoffee2 diskon 50%!
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions className={classes.cardActions}>
                <Box className={classes.author}>
                  <Box ml={2}>
                    <Typography variant="subtitle2" component="p">
                      Johanes Simarmata
                    </Typography>
                    <Typography variant="subtitle2" color="textSecondary" component="p">
                      2 May 2021
                    </Typography>
                  </Box>
                </Box>
                <Box>
                  <CommentIcon></CommentIcon>
                </Box>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={12} sm={6} md={4}>
            <Card className={classes.card}>
              <CardActionArea>
                <CardMedia className={classes.media} image={coffee} title="Coffee" />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    MyCoffee3
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    Promo 2 minggu kedepan! MyCoffee3 diskon 70%!
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions className={classes.cardActions}>
                <Box className={classes.author}>
                  <Box ml={2}>
                    <Typography variant="subtitle2" component="p">
                      Johanes Simarmata
                    </Typography>
                    <Typography variant="subtitle2" color="textSecondary" component="p">
                      3 May 2021
                    </Typography>
                  </Box>
                </Box>
                <Box>
                  <CommentIcon></CommentIcon>
                </Box>
              </CardActions>
            </Card>
          </Grid>
          <Grid item xs={12} sm={6} md={4}>
            <Card className={classes.card}>
              <CardActionArea>
                <CardMedia className={classes.media} image={coffee} title="Coffee" />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    MyCoffee4
                  </Typography>
                  <Typography variant="body2" color="textSecondary" component="p">
                    Promo 3 minggu kedepan! MyCoffee2 diskon 60%!
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions className={classes.cardActions}>
                <Box className={classes.author}>
                  <Box ml={2}>
                    <Typography variant="subtitle2" component="p">
                      Johanes Simarmata
                    </Typography>
                    <Typography variant="subtitle2" color="textSecondary" component="p">
                      4 May 2021
                    </Typography>
                  </Box>
                </Box>
                <Box>
                  <CommentIcon></CommentIcon>
                </Box>
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export default App;

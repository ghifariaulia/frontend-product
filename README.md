# Cafetiere Application

## Group Member

- Nofaldi Fikrul Atmam
- Johanes Marihot Perkasa Simarmata
- Hasna Nadifah
- Piawai Said Umbara
- Ghifari Aulia Azhar Riza

## Feature

- Authentication
- Notification
- Order / Buy Product
- CRUD Product (admin)
- Testimonials
- Post

## Production Notes

- [Deployed Website](https://cafetiere.vercel.app/)

## Local Development Notes

Before you start developing in local don't forget to download node.js first.

```js
npm install
npm start
```

## Creating / Updating a Feature

- when you are working on a feature (bug, new feature, refractor, etc).
- always create a new branch with this naming conventions `{your-name}/{branch-name}` example `nofaldi/refractor-auth`.
